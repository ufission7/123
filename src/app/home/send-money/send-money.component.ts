import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HomeService } from '../home.service';
import {faUser} from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';
import { MoneyTransfer } from './moneyTransfer';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
@Component({
  selector: 'app-send-money',
  templateUrl: './send-money.component.html',
  styleUrls: ['./send-money.component.scss']
})
export class SendMoneyComponent implements OnInit {


userForm:FormGroup;
userAmount:FormGroup;
faUser = faUser;
userName:string;
userId:Number;
btnStyle: string;
amountStyle:string;
userEmail : string;
userGendar : string;
moneyTransfer:MoneyTransfer;
  constructor(private homeService:HomeService,private toastr:ToastrService,public dialog: MatDialog,){
    this.btnStyle = 'userInfoHide';
    this.amountStyle = 'amountHide';
   }

  ngOnInit() {
    console.log('init')
    this.userForm = new FormGroup({
      userName : new FormControl('',Validators.required)
        });
        this.userAmount = new FormGroup({
          amount: new FormControl('',Validators.required)
        });

        this.moneyTransfer = {
         userName : '',
         amount : ''
        }
        
  }
  
  getUserName(){
   
   this.homeService.getUserDetails(this.userForm.get('userName').value).subscribe(data=>{
     this.btnStyle = 'userInfoShow';
     this.amountStyle = 'amountShow';
    this.userId =data.userId
    this.userName = data.userName;
    this.userEmail = data.email;
    this.userGendar = data.gender;
     
   },()=>{
    this.toastr.error("User Not Found..");
});
  }

  sendMoney(){
  
    this.moneyTransfer.amount = this.userAmount.get('amount').value;
    this.moneyTransfer.userName = this. userName;
    this.homeService.sendMoneyToUser(this.moneyTransfer).subscribe(data=>{
     if(data=='Insufficient Balance')
     {
       this.toastr.error('Insufficient Balance')
     }
     else
     {
     this.toastr.success('Money Send Successfully')
     setTimeout(() => {
      self.close();
     }, 3000);
     }
    },()=>{
      this.toastr.error("Error Sending Money")
    });
  }

  
}
