import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HomeComponent } from '../home.component';
import { HomeService } from '../home.service';
import { WithdrawRequest } from './withdrawRequest';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss']
})
export class WithdrawComponent implements OnInit {

  withdrawForm:FormGroup;
withdrawReq:WithdrawRequest;
balance:number;
amount:number;

  constructor(private homeService:HomeService,private toaster:ToastrService) {
    this.withdrawReq={
      name:'',
      accNo:'',
      ifsc:'',
      upiId:'',
      phonenumber:undefined,
      city:'',
      amount:''
    }
   }

  ngOnInit() {
    this.homeService.getUserBalance().subscribe(data=>{
this.balance=data;
    });
    this.withdrawForm = new FormGroup({
      name : new FormControl('',Validators.required),
      accNo : new FormControl('',Validators.required),
      ifsc : new FormControl('',Validators.required),
      upiId : new FormControl('',Validators.required),
      phonenumber : new FormControl('',Validators.required),
      city : new FormControl('',Validators.required),
      amount:new FormControl('',Validators.required)
    })
  }

  Withdraw(){
    console.log("bal........."+this.balance)
    this.withdrawReq.name = this.withdrawForm.get('name').value;
    this.withdrawReq.accNo = this.withdrawForm.get('accNo').value;
    this.withdrawReq.ifsc = this.withdrawForm.get('ifsc').value;
    this.withdrawReq.upiId = this.withdrawForm.get('upiId').value;
    this.withdrawReq.phonenumber = this.withdrawForm.get('phonenumber').value;
    this.withdrawReq.city = this.withdrawForm.get('city').value;
this.withdrawReq.amount = this.withdrawForm.get('amount').value;
  
if(parseInt(this.withdrawReq.amount)<100)
{
  this.toaster.error('Enter Amount greater than 100')
}
else if(this.balance - (parseInt(this.withdrawReq.amount)- parseInt(this.withdrawReq.amount)*3/100)<0)
{
 
  this.toaster.error('Insufficient balance! Please reacharge your account')
}
else{

  this.withdrawReq.amount = (parseInt(this.withdrawReq.amount) - parseInt(this.withdrawReq.amount)*3/100).toString();
this.homeService.withdrawalRequest(this.withdrawReq).subscribe(data=>{
this.toaster.success('Withdraw Request Submitted Successfully');
setTimeout(() => {
 self.close();
}, 3000);
},()=>{
  this.toaster.error('Error Submitting Withdraw Request')
})
  }
}

getAmountHint()
{
 
  if(isNaN(parseFloat(this.amount.toString())))
  return "Enter Number"
  else if(this.amount<100)
  return "Enter amount greater than 100"
  else if((this.balance - (this.amount-this.amount*3/100))<0)
  return "Insufficient balance"
  else if(this.amount==0 || this.amount==undefined)
  return ""
  else
  return this.amount-this.amount*3/100+"rs will be withdrawn(3% additional charge)"
}
}
