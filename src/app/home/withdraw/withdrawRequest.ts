export interface WithdrawRequest
{
    name:string,
    accNo:string,
    ifsc:string,
    upiId:string,
    phonenumber:number,
    city:string
    amount:string;
}