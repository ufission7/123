import { Component, OnInit,NgZone, Output, EventEmitter } from '@angular/core';
import {MatDialog} from '@angular/material';
import {ConfirmDialogComponent} from '../shared/confirm-dialog/confirm-dialog.component';
import { HomeService } from './home.service';
import { AuthService } from '../shared/auth.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // balance:number;
  balance:number;
username:string;
isSubscribed:boolean;
adminRole:boolean;
  constructor(
    
    private homeService:HomeService,
    private authService:AuthService,
    public dialog: MatDialog,
    public toastr:ToastrService
  ) {
   
  }



  ngOnInit() {
    this.homeService.getUserBalance().subscribe(data=>{

      this.balance = data;
     
      });
    this.homeService.getUserRole(this.authService.getUserName()).subscribe(data=>{
      this.adminRole=data;
    });
    this.username = this.authService.getUserName();
   
    this.homeService.getUserSubscribed(this.authService.getUserName()).subscribe(data=>{
      this.isSubscribed = data;
    })
    
  }



  initPay():boolean{
  if(this.balance<200)
  {
    this.toastr.error("Insufficient balace, 200rs needed for subscription");
    return false;
  }
  else{
    // let's call our modal window
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "1400px",
      data: {
          title: "Are you sure?",
          message:"200rs will be debited from your wallet"
        }
    });
  
   
    dialogRef.afterClosed().subscribe(dialogResult => {

      if(dialogResult==true){
     
     this.homeService.addUserSubscription(this.username).subscribe(data=>{
    
         this.toastr.success('Subscription Added!')

         setTimeout(() => {
          location.reload();
         }, 1500);
       
     },()=>{
       this.toastr.error('Subscription Failed.')
     })
      
   }
  });
}
  }

  
}

//   getOrderId():string
//   {
//     this.paymentService.getOrderId(this.paymentPayload).subscribe(data=>{
//       this.orderObj = data;
//       console.log("orderObj "+this.orderObj)
//     });
//     console.log("orderId "+this.orderObj.id)
//     return this.orderObj.id;
//   }



