import { Component, OnInit, ViewChild } from '@angular/core';
import { HomeService } from '../home.service';
import { TransactionItems } from './transactionItems';
import { Observable} from 'rxjs';
import { MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-transaction-details',
  templateUrl: './transaction-details.component.html',
  styleUrls: ['./transaction-details.component.scss']
})
export class TransactionDetailsComponent implements OnInit {

  Items:Observable<any[]>;
  balance:number;

  constructor(private homeService:HomeService) { 
  }

  ngOnInit() {
    this.homeService.getUserBalance().subscribe(data=>{

      this.balance = data;
      });
    this.homeService.getTransactionDetailsForUser().subscribe(data=>{
      console.log(data)
      if(data!=null)
      this.Items = data;
    })
  }

  displayedColumns: string[] = ['sno', 'customerName', 'amount', 'status','balance'];
  // dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  // @ViewChild(MatPaginator) paginator: MatPaginator;

  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  // }

  

}

