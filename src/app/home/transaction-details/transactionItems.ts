export interface TransactionItems{
    sno:number,
    receiver:string,
    amountSend:number,
    status:string
}