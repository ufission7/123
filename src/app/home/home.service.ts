import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, Output } from '@angular/core';

import { Observable} from 'rxjs';
import { environment } from 'src/environments/environment';
import { MoneyTransfer } from './send-money/moneyTransfer';
import { AddMoney } from './wallet/addMoney';
import { WithdrawRequest } from './withdraw/withdrawRequest';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  endpoint:string;
  
  
  constructor(private httpClient:HttpClient) {
    this.endpoint = environment.api_url;
   }

  

  getUserBalance() : Observable<any>{
    return this.httpClient.get(this.endpoint+'/acc/balance');
  }

  // signup(signupRequest : SignUpRequest) : Observable<any>{
  //   return this.httpClient.post('http://localhost:8081/auth/signUp',signupRequest,{ responseType:'text'});
  //     }

  getUserDetails(userName:string):Observable<any>{
    return this.httpClient.get(this.endpoint+'/search/user/'+userName.toLowerCase())
  }

  sendMoneyToUser(moneyTransfer :MoneyTransfer):Observable<any>{
    
    return this.httpClient.post(this.endpoint+'/acc/sendMoney',moneyTransfer,{ responseType:'text'});
  }

  addMoneyToWallet(addMoney:AddMoney):Observable<any>
  {
    console.log("addding money")
   return this.httpClient.post(this.endpoint+'/acc/addMoney',addMoney,{responseType:'text'});
  }

  getTransactionDetailsForUser():Observable<any>{
    return this.httpClient.get(this.endpoint+'/acc/transactionDetails');
  }

  getUserRole(username:string):Observable<any>{
    
return this.httpClient.get(this.endpoint+'/auth/checkRole?username='+username);
  }

  withdrawalRequest(request:WithdrawRequest):Observable<any>
  {
    return this.httpClient.post(this.endpoint+'/acc/submitWithdrawRequest',request);
  }

  getKycStatus(username:string):Observable<any>
  {
    return this.httpClient.get(this.endpoint+'/acc/kycStatus?username='+username,{responseType:'text'});
  }
  addUserSubscription(username:string):Observable<any>
  {
    return this.httpClient.get(this.endpoint+'/acc/addUserSubscription?username='+username,{responseType:'text'});
  }
  getNoOfTransaction(username:string)
  {
     return this.httpClient.get(this.endpoint+'/acc/checkNoOfTransaction?username='+username);
  }
  getUserSubscribed(username:string):Observable<any>
{
  return this.httpClient.get(this.endpoint+'/acc/checkUserSubscribed?username='+username);
}
}