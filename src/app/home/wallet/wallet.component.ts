import { Component, NgZone, OnInit } from '@angular/core';
import { InternalNgModuleRef } from '@angular/core/src/linker/ng_module_factory';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PaymentPayload } from 'src/app/payment-payload';
import { RazorpayResponse } from 'src/app/payment-response';
import { PaymentService } from 'src/app/payment.service';
import { AuthService } from 'src/app/shared/auth.service';
import { SendMoneyDialogComponent } from 'src/app/shared/send-money-dialog/send-money-dialog.component';
import { WithdrawDialogComponent } from 'src/app/shared/withdraw-dialog/withdraw-dialog.component';
import { CustomWindow, WindowRefService } from 'src/app/window-ref.service';
import { HomeService } from '../home.service';
import { AddMoney } from './addMoney';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {

  balance:number;
  amountToAdd:FormGroup;
  data:string;
  addMoneyToWallet:AddMoney;
  paymentPayload:PaymentPayload;
paymentResponse:RazorpayResponse;
orderId:string;
orderObj={
  id:''
}
transactionCheck:boolean;

  constructor(
    private homeService:HomeService,
    private zone: NgZone,
    private winRef: WindowRefService,
    private paymentService:PaymentService,
    private route:Router,
    private authService:AuthService,
    private toastr:ToastrService,
    public dialog: MatDialog,
  ) {
    this._window = this.winRef.nativeWindow;
    this.paymentPayload = {
      amount : '',
      currency : ''
    }
    this.paymentResponse = {
      razorpay_payment_id:'',
   razorpay_order_id:'',
     razorpay_signature:''
    };
    this.addMoneyToWallet = {
      amount : '',
      userName : ''
    }
  }


  ngOnInit() {


    this.homeService.getUserBalance().subscribe(data=>{

this.balance = data;
});
this.amountToAdd = new FormGroup({
  amount : new FormControl('',Validators.required)
    });

    this.homeService.getNoOfTransaction(this.authService.getUserName()).subscribe(data=>{
      if(data<10)
      this.transactionCheck=false;
      
      console.log("transaction..."+this.transactionCheck)
    });
}

addMoney(){
this.initPay(((parseInt(this.amountToAdd.get('amount').value))*100).toString());
}


private _window: CustomWindow;
public rzp: any;

public options = {
  key: 'rzp_live_sqpQJ8U9sb9UH6', 
  name: 'Ufission',
  description: 'Subscribe',
  amount:'',
   order_id:'',
  prefill: {
    name: 'Ufission',
    email: '', 
  },
  notes: {},
  theme: {
    color: '#3880FF'
  },
  handler: this.paymentHandler.bind(this),
  modal: {
    ondismiss: (() => {
      this.zone.run(() => {
        // add current page routing if payment fails
      })
    })
  }
};


initPay(amount:string): void {
  this.paymentPayload.amount = amount;
  this.paymentPayload.currency = 'INR';
this.paymentService.getOrderId(this.paymentPayload).subscribe(data=>{
this.orderObj = data;
 this.options.order_id = this.orderObj.id;
this.options.amount = this.paymentPayload.amount; 
 console.log("order id "+this.options.order_id)
console.log("amount " +this.options.amount)
  this.rzp = new this.winRef.nativeWindow['Razorpay'](this.options);
  this.rzp.open();
});

}

paymentHandler(res: any) {
  
  this.zone.run(() => {
    this.paymentResponse.razorpay_payment_id = res.razorpay_payment_id
    this.paymentResponse.razorpay_order_id = res.razorpay_order_id;
    this.paymentResponse.razorpay_signature = res.razorpay_signature;
   
    this.paymentService.verifySignature(this.paymentResponse).subscribe(data=>{
this.addMoneyToWallet.amount = (parseInt(this.paymentPayload.amount)/100).toString();
this.addMoneyToWallet.userName = this.authService.getUserName();

     this.homeService.addMoneyToWallet(this.addMoneyToWallet).subscribe(data=>{
       this.route.navigateByUrl('/home');
       if(data=='Invalid Login')
       this.toastr.error(data);
       else
           this.toastr.success(data);      
     }
     );
    },
    ()=>
    {
      this.toastr.error("Payment Unsuccesfull,Please retry!");
      console.log("error")
    }
    // add API call here
  );
});
}
sendMoney()
{
  const dialogRef = this.dialog.open(SendMoneyDialogComponent, {
    maxWidth: "1400px"
  });

 
  dialogRef.afterClosed().subscribe(dialogResult => {
});
}
withdrawMoney()
{
  const dialogRef = this.dialog.open(WithdrawDialogComponent, {
    maxWidth: "1400px"
  });

 
  dialogRef.afterClosed().subscribe(dialogResult => {
});
}
}

