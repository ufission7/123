import { Injectable } from '@angular/core';
import { 
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';
import { AuthService } from './auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
@Injectable()
export class RoleGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router,private jwtHelperService: JwtHelperService,private authService:AuthService) {}
  canActivate(route: ActivatedRouteSnapshot): boolean {
    
    const requestedRoute  = route.routeConfig.path;
    const expectedRole = route.data.expectedRole;

    const expectedName= route.data.expectedName;

    const authenticationtoken =this.auth.getJwtToken();

  const decodeToken = this.jwtHelperService.decodeToken(authenticationtoken);

  // check if it was decoded successfully, if not the token is not valid, deny access
    if (!decodeToken) {

      return false;
    }
    
    if(requestedRoute==='admin/withdrawalRequests')
    {
      if(expectedName==='mohan')
      return true;
      else
      return false;
    }
    if (
      !this.auth.isAuthenticated())
      {
        this.authService.logout();
        this.router.navigate(['login'])
        return false;
      }
      if(
      decodeToken['AUTHORITIES_KEY']!== expectedRole)
     {
      this.router.navigate(['home']);
      return false;
      
    }

    return true;
  
  // check if the user roles is in the list of allowed roles, return true if allowed and false if not allowed
   
    // if (
    //   !this.auth.isAuthenticated() || 
    //   tokenPayload.role !== expectedRole
    // ) {
    //   this.router.navigate(['login']);
    //   return false;
    // }
    return true;
  }
}