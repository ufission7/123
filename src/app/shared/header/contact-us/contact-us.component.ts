import { Component, OnInit } from '@angular/core';
import {faMailBulk,faPhone} from '@fortawesome/free-solid-svg-icons';
import{faFacebookSquare,faTwitter,faInstagram} from '@fortawesome/free-brands-svg-icons'
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
faMailBulk = faMailBulk;
faFacebook=faFacebookSquare;
faPhone=faPhone;
faTwitter=faTwitter;
faInstagram=faInstagram;

  constructor() { }

  ngOnInit() {
  }

}
