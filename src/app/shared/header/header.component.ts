import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {faBars,faUser} from '@fortawesome/free-solid-svg-icons';
import { ProfileService } from 'src/app/profile/profile.service';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public isCollapsed = true;
  faBars = faBars;
  faUser = faUser;
  isLoggedIn : boolean;
  username : string;

  constructor(private authService:AuthService,private router:Router,private profileService:ProfileService) { 
    
  }

  ngOnInit() {
    this.authService.loggedIn.subscribe((data: boolean) => this.isLoggedIn = data);
    this.authService.username.subscribe((data: string) => this.username = data);
  
    this.isLoggedIn = this.authService.isLoggedIn();
    this.username = this.authService.getUserName();
    console.log(this.isLoggedIn)
  }
  logout() {
    this.authService.logout();
    this.isLoggedIn = false;
    this.router.navigateByUrl('/login');
  }

  goToUserProfile()
  {
    this.router.navigate(['home/editProfile']);
  }
  goToChangePassword()
  {
    this.router.navigate(['home/changePassword']);
  }


}

