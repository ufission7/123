import { Injectable } from '@angular/core';
import { Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from './auth.service';
@Injectable()
export class AuthGuardService implements CanActivate {
   NotaccessibleUrl = [ '/paymentsuccess'];
  constructor(public auth: AuthService, public router: Router,private authService:AuthService,private toaster:ToastrService) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const requestedRoute  = next.routeConfig.path;
    console.log(requestedRoute)
    if(requestedRoute==='login')
    {
      if(!this.authService.isLoggedIn())
      return true;
      else
      {
        this.router.navigate(['home']);
        return false;
      }
    }
    if(requestedRoute==='sign-up')
    {
      if(!this.authService.isLoggedIn())
      return true;
      else
      {
        this.router.navigate(['home']);
        return false;
      }
    }
    if(!this.authService.isLoggedIn())
    {
    
      this.toaster.error("Please Login");
      this.router.navigate(['/login']);
    return false;
    }
    if(this.checkUrlNotAccessible(state.url))
    {
      if(this.auth.isAuthenticated()){
      this.router.navigate(['home']);
      return false;
    }
      else{
       
        alert("User Session Expired..")
        this.authService.logout();
        this.router.navigate(['login']);
        return false;
      }
    }
    if (!this.auth.isAuthenticated()) {
      alert("User Session Expired..")
      this.authService.logout();
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
  checkUrlNotAccessible(url:string):boolean{
if(this.NotaccessibleUrl.indexOf(url)>-1)
return true;
else
return false;
}

}