import { EventEmitter, Injectable, Output } from '@angular/core';
import { Observable,throwError} from 'rxjs';
import { SignUpRequest } from '../sign-up/signup-request';
import {HttpClient} from '@angular/common/http'
import {LoginRequest} from '../login/login-request'
import { LocalStorageService } from 'ngx-webstorage';
import { LoginResponse } from '../login/login-response';
import {tap,map} from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import 'rxjs/add/operator/map'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  @Output() loggedIn: EventEmitter<boolean> = new EventEmitter();
  @Output() username: EventEmitter<string> = new EventEmitter();


endpoint:string;
  constructor(private httpClient : HttpClient,private localStorage:LocalStorageService,private jwtHelper : JwtHelperService) {
    this.endpoint = environment.api_url;
   }

  refreshTokenPayload = {
    refreshToken: this.getRefreshToken(),
    username: this.getUserName()
  }

  signup(signupRequest : SignUpRequest) : Observable<any>{
    return this.httpClient.post(this.endpoint+'/auth/signUp',signupRequest,{ responseType:'text'});
      }

      login(loginRequestPayload: LoginRequest): Observable<boolean> {
    
        return this.httpClient.post<LoginResponse>(this.endpoint+'/auth/login',
          loginRequestPayload).pipe(map(data => {
            this.localStorage.store('authenticationToken', data.token);
            this.localStorage.store('username', data.username);
            this.localStorage.store('refreshToken', data.refreshToken);
            this.localStorage.store('expireTime', data.expireTime);
            this.loggedIn.emit(true);
            this.username.emit(this.getUserName());
            return true;
          }));
      }
      getJwtToken()
      {
        return this.localStorage.retrieve('authenticationToken');
      }
      refreshToken() {
        return this.httpClient.post<LoginResponse>(this.endpoint+'/auth/refresh/token',
          this.refreshTokenPayload)
          .pipe(tap(response => {
            this.localStorage.clear('authenticationToken');
            this.localStorage.clear('expireTime');
          
    
            this.localStorage.store('authenticationToken',
              response.token);
            this.localStorage.store('expireTime', response.expireTime);
          }));
      }
    
      getUserName() {
        return this.localStorage.retrieve('username');
      }
      getRefreshToken() {
        return this.localStorage.retrieve('refreshToken');
      }
 
      logout() {
        this.httpClient.post(this.endpoint+'/auth/logout', this.refreshTokenPayload,
          { responseType: 'text' })
          .subscribe(data => {
            console.log(data);
          }, error => {
            throwError(error);
          })
        this.localStorage.clear('authenticationToken');
        this.localStorage.clear('username');
        this.localStorage.clear('refreshToken');
        this.localStorage.clear('expireTime');
        this.loggedIn.emit(false);
      }

   isAuthenticated(): boolean 
   {
      const token = this.localStorage.retrieve('authenticationToken');
     
      console.log("token expired "+this.jwtHelper.isTokenExpired(token,parseInt(localStorage.getItem('expiretime'))));
     
     
      return !this.jwtHelper.isTokenExpired(token,parseInt(localStorage.getItem('expiretime'))) ;

    }

    isLoggedIn(): boolean {
      // console.log("login status "+this.getJwtToken()!==null || this.getJwtToken()!=='undefined')
      return this.getJwtToken()!==null && this.getJwtToken()!=='undefined';
    }

    postFile(signupRequest : SignUpRequest,fileToUpload: File){

      const endpoint = this.endpoint+'/auth/uploadImage';
      const formData: FormData = new FormData();
      formData.append("file", fileToUpload, fileToUpload.name);
    formData.append("signup",JSON.stringify(signupRequest));
      return this.httpClient.post(endpoint,formData).subscribe(data=>{

      },()=>{
        console.log("error")
      });
  }

  checkUserExist(username:string):Observable<any>
  {
      return this.httpClient.get(this.endpoint+'/auth/checkIfUserExist?username='+username);
  
  }
  checkAadharExist(aadhar:string):Observable<any>
  {
     return this.httpClient.get(this.endpoint+'/auth/checkAadharExist?aadhar='+aadhar);
  
  }
}