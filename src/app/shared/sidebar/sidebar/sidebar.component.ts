import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input()
   mainTemplate: TemplateRef<any>;
  @Input()
  adminRole:boolean;
  @Input()
  sideBarTemplate:TemplateRef<any>;
  homeStatus:boolean=false;
  walletStatus:boolean=false;
  transactionStatus:boolean = false;
  public isCollapsed = true;
public isOpened = false
  faBars = faBars;

  constructor() { }

  ngOnInit() {
   
  }

  home(){
    this.homeStatus = !this.homeStatus;
    console.log("homeStatus"+this.homeStatus)
   }
   wallet(){
     this.walletStatus = !this.walletStatus;
   }
   transaction(){
     this.transactionStatus = !this.transactionStatus;
   }




}
