import { Component, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Observable } from 'rxjs';
import { AdminService } from '../admin.service';
import { saveAs } from 'file-saver';
import { base64StringToBlob } from 'blob-util';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';


@Component({
  selector: 'app-admin-kyc-request',
  templateUrl: './admin-kyc-request.component.html',
  styleUrls: ['./admin-kyc-request.component.scss']
})
export class AdminKycRequestComponent implements OnInit {

  Items:Observable<any[]>;
  username:string;
  
  constructor(private adminService:AdminService,private kycService:AdminService,private toaster:ToastrService,private router:Router,private authService:AuthService) {
    this.username = this.authService.getUserName();
   }

  ngOnInit() {
 this.adminService.getKycRequests().subscribe(data=>{
  this.Items = data;
  
//  let filename='aadhar.pdf';
//  saveAs(new Blob([data[7]['aadharImage']],{type: "application/pdf;charset=utf-8"}),filename);
// let blob = base64StringToBlob(data[8]['aadharImage'],type);
// saveAs(blob, "aadhar.pdf",{ autoBOM: true });

})


  }

  downloadAadhar(user:Observable<any>)
  {
    // let type='application/pdf;charset=utf-8';
    let type='image/jpg';
    let blob = base64StringToBlob(user['aadharImage'],type);
 saveAs(blob, user['username']+" aadhar.jpg",{ autoBOM: true });
  }

  // open(content: TemplateRef<any>) {
  //   const ref = this.overlayService.open("Hello World", null);
  //   // const ref = this.overlayService.open(content, null);
  //   ref.afterClosed$.subscribe(res => {
  //    console.log(res);
  //   });
  // }


removeRow(userDetails:Observable<any>,status:string){
 
this.adminService.updateKyc(userDetails['username'],status).subscribe(data=>{
  if(status=='Approved')
  {
    this.toaster.success("Application Approved")
  }
else
{
  this.toaster.error("Application Rejected")
}
location.reload();
},()=>{
  this.toaster.error("error while updating the kyc request")
});
}
  }



