import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminKycRequestComponent } from './admin-kyc-request.component';

describe('AdminKycRequestComponent', () => {
  let component: AdminKycRequestComponent;
  let fixture: ComponentFixture<AdminKycRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminKycRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminKycRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
