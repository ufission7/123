import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { AdminService } from '../admin.service';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-admin-withdrawal-request',
  templateUrl: './admin-withdrawal-request.component.html',
  styleUrls: ['./admin-withdrawal-request.component.scss']
})
export class AdminWithdrawalRequestComponent implements OnInit {

  
  Items:Observable<any[]>;
  username:string;
  constructor(private adminService:AdminService,private toaster:ToastrService,private authService:AuthService) {
    this.username = this.authService.getUserName();
   }

  ngOnInit() {
    this.adminService.getWithdrawRequest().subscribe(data=>{
      this.Items = data;
    })
  }

  removeRow(userDetails:Observable<any>,status:string){
 
    this.adminService.updateWithdrawReq(userDetails['username'],userDetails['id'],status).subscribe(data=>{
      if(status=='Paid')
      {
        this.toaster.success("Application Approved");
        setTimeout(() => {
          location.reload();
      }, 1500);
        
      }
   
    },()=>{
      this.toaster.error("error while updating the withdraw request")
    });
    }

}
