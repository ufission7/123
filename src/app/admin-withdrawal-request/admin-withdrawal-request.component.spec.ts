import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWithdrawalRequestComponent } from './admin-withdrawal-request.component';

describe('AdminWithdrawalRequestComponent', () => {
  let component: AdminWithdrawalRequestComponent;
  let fixture: ComponentFixture<AdminWithdrawalRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWithdrawalRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWithdrawalRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
