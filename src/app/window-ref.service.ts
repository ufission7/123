import { Injectable } from '@angular/core';

export interface CustomWindow extends Window{
  __custom_global_windows:string;
}
function getWindow():any{
  return window;
}

@Injectable({
  providedIn: 'root'
})
export class WindowRefService {

  constructor() { }

  get nativeWindow():CustomWindow
  {
    return getWindow();
  }
}
