import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PaymentPayload } from './payment-payload';
import { RazorpayResponse } from './payment-response';

@Injectable({
  providedIn: 'root'
})

export class PaymentService {

endpoint:string;

  constructor(private httpClient:HttpClient) {
    this.endpoint = environment.api_url;
   }

  getOrderId(PaymentPayload:PaymentPayload):Observable<any>
  {
   
    return this.httpClient.post(this.endpoint+'/api/payment/CreateOrder',PaymentPayload);
  }
  verifySignature(razorpayResponse:RazorpayResponse):Observable<any>
{
  
  return this.httpClient.post(this.endpoint+'/api/payment/verifySignature',razorpayResponse,{responseType: 'text'});

}
}
