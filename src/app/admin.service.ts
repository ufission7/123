
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class AdminService {

endpoint:string;
  constructor(private httpClient : HttpClient) {
    this.endpoint=environment.api_url;
   }

  getKycRequests():Observable<any>{
    return this.httpClient.get(this.endpoint+'/admin/kycRequests')
  }

  updateKyc(username:string,status:string){

    const endpoint = this.endpoint+'/admin/updateKyc';
    const formData: FormData = new FormData();
    formData.append("username",username);
  formData.append("status",status);

     return this.httpClient.post(endpoint,formData);

}

getWithdrawRequest():Observable<any>{
  return this.httpClient.get(this.endpoint+'/admin/withdrawRequest')
}

updateWithdrawReq(username:string,id:string,status:string){

  const endpoint = this.endpoint+'/admin/updateWithdrawReq';
  const formData: FormData = new FormData();
  formData.append("username",username);
formData.append("status",status);
formData.append("id",id);
console.log("withdraw")
   return this.httpClient.post(endpoint,formData);

}

}
