import { Component, NgZone } from '@angular/core';
import { CustomWindow, WindowRefService } from './window-ref.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ufission-js';

}