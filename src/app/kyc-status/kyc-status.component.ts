import { Component, OnInit } from '@angular/core';
import { HomeService } from '../home/home.service';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-kyc-status',
  templateUrl: './kyc-status.component.html',
  styleUrls: ['./kyc-status.component.scss']
})
export class KycStatusComponent implements OnInit {
  balance:number;
  kycStatus:string;
  constructor(private homeService:HomeService,private authService:AuthService) { }

  ngOnInit() {
    this.homeService.getUserBalance().subscribe(data=>{

      this.balance = data;
      });
this.homeService.getKycStatus(this.authService.getUserName()).subscribe(data=>{
 this.kycStatus = data;
 console.log(this.kycStatus)
})
  }

}
