import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import {AuthGuardService as AuthGuard, AuthGuardService} from './shared/auth-guard.service'
import { HeaderComponent } from './shared/header/header.component';
import { NotifyComponent } from './notify/notify.component';
import { WalletComponent } from './home/wallet/wallet.component';
import { SendMoneyComponent } from './home/send-money/send-money.component';
import { TransactionDetailsComponent } from './home/transaction-details/transaction-details.component';
import { 
  RoleGuardService as RoleGuard 
} from './shared/role-gaurd.service';
import { AdminKycRequestComponent } from './admin-kyc-request/admin-kyc-request.component';
import { AdminWithdrawalRequestComponent } from './admin-withdrawal-request/admin-withdrawal-request.component';
import { WithdrawComponent } from './home/withdraw/withdraw.component';
import { KycStatusComponent } from './kyc-status/kyc-status.component';
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';
import { ChangePasswordComponent } from './profile/change-password/change-password.component';
import { SubscribeComponent } from './home/subscribe/subscribe.component';
import { ReferralTreeComponent } from './referral-tree/referral-tree.component';
import { ContactUsComponent } from './shared/header/contact-us/contact-us.component';

const routes: Routes = [
  {path:'',redirectTo:'login',pathMatch: 'full'},
  {path:'login',component:LoginComponent,canActivate: [AuthGuard]},
  {path:'sign-up',component:SignUpComponent,canActivate: [AuthGuard]},
  {path:'home',component:HomeComponent,canActivate: [AuthGuard] },
  {path:'home/wallet',component:WalletComponent,canActivate: [AuthGuard]},
  {path:'paymentsuccess',component:NotifyComponent,canActivate: [AuthGuard]},
  {path:'home/sendMoney',component:SendMoneyComponent,canActivate: [AuthGuard]},
  {path:'home/transactionDetails',component:TransactionDetailsComponent,canActivate: [AuthGuard]},
  {path:'admin/kycRequests',component:AdminKycRequestComponent,canActivate: [RoleGuard],data: { 
    expectedRole: 'ADMIN'}},
    {path:'admin/withdrawalRequests',component:AdminWithdrawalRequestComponent,canActivate: [RoleGuard],data: { 
      expectedName: 'mohan'}},
      {path:'home/withdrawMoney',component:WithdrawComponent,canActivate: [AuthGuard]},
      {path:'home/kycStatus',component:KycStatusComponent,canActivate: [AuthGuard]},
      {path:'home/editProfile',component:EditProfileComponent,canActivate: [AuthGuard]},
      {path:'home/changePassword',component:ChangePasswordComponent},
      {path:'home/subscribe',component:SubscribeComponent,canActivate:[AuthGuard]},
      {path:'home/referalTree',component:ReferralTreeComponent,canActivate:[AuthGuard]},
      {path:'contact-us',component:ContactUsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
