import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './shared/header/header.component';
import { TokenInterceptor } from './token-interceptor';
import {NgxWebstorageModule} from 'ngx-webstorage';
import { HomeComponent } from './home/home.component';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { JwtModule,JWT_OPTIONS} from '@auth0/angular-jwt';
import { JwtHelperService } from '@auth0/angular-jwt';
import {AuthGuardService} from './shared/auth-guard.service';
import { NotifyComponent } from './notify/notify.component';
import { WalletComponent } from './home/wallet/wallet.component';
import { SidebarComponent } from './shared/sidebar/sidebar/sidebar.component';
import { SendMoneyComponent } from './home/send-money/send-money.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { TransactionDetailsComponent } from './home/transaction-details/transaction-details.component';
import { RoleGuardService } from './shared/role-gaurd.service';
import { AdminKycRequestComponent } from './admin-kyc-request/admin-kyc-request.component';
import { AdminWithdrawalRequestComponent } from './admin-withdrawal-request/admin-withdrawal-request.component';
import { WithdrawComponent } from './home/withdraw/withdraw.component';
import { KycStatusComponent } from './kyc-status/kyc-status.component';
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';
import { ChangePasswordComponent } from './profile/change-password/change-password.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { MatButtonModule, MatCardModule, MatDatepickerModule, MatIconModule, MatListModule, MatNativeDateModule, MatPaginator, MatPaginatorModule, MatSidenavModule, MatTableModule } from '@angular/material';
import { SubscribeComponent } from './home/subscribe/subscribe.component';
import { ConfirmDialogComponent } from './shared/confirm-dialog/confirm-dialog.component';
import { ReferralTreeComponent } from './referral-tree/referral-tree.component';
import { ContactUsComponent } from './shared/header/contact-us/contact-us.component';
import { SendMoneyDialogComponent } from './shared/send-money-dialog/send-money-dialog.component';
import { WithdrawDialogComponent } from './shared/withdraw-dialog/withdraw-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    LoginComponent,
    HeaderComponent,
    HomeComponent,
    NotifyComponent,
    WalletComponent,
    SidebarComponent,
    SendMoneyComponent,
    TransactionDetailsComponent,
    AdminKycRequestComponent,
    AdminWithdrawalRequestComponent,
    WithdrawComponent,
    KycStatusComponent,
    EditProfileComponent,
    ChangePasswordComponent,
    SubscribeComponent,
    ConfirmDialogComponent,
    ReferralTreeComponent,
    ContactUsComponent,
    SendMoneyDialogComponent,
    WithdrawDialogComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxWebstorageModule.forRoot(),
    NgbCollapseModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut:2000
    }),
    JwtModule,
    Ng2SearchPipeModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatButtonModule
  ],
  entryComponents: [ConfirmDialogComponent,SendMoneyDialogComponent,WithdrawDialogComponent],
  providers: [
    {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
    },
    {
      provide:JWT_OPTIONS,useValue:JWT_OPTIONS
    },
    JwtHelperService,
    AuthGuardService,
    RoleGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
