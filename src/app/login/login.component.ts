import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { LoginRequest } from './login-request';
import Typewriter from 't-writer.js'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginRequest:LoginRequest;
  loginForm :FormGroup;
 

  constructor(private authService : AuthService,private router:Router,private toaster:ToastrService,private activatedRoute : ActivatedRoute) {
     console.log("came to login")
    this.loginRequest={
      username: '',
      password : ''
    }
   }


  ngOnInit() {
    this.loginForm = new FormGroup({
      username : new FormControl('',Validators.required),
   
      password : new FormControl('',Validators.required),
    
    })
  
    const target = document.querySelector('.tw');
    const writer = new Typewriter(target, {
      loop: true,
      typeSpeed: 150,
      deleteSpeed: 150,
      typeColor: '#44318D'
    })
    
    writer
      .type('Ufission')
      .rest(300)
      .changeOps({ deleteSpeed: 150 })
      .remove(8)
      .type('Future Within You')
      .clear()
      .start()
  }

  login()
  {
    
    this.loginRequest.username =  this.loginForm.get('username').value;
    this.loginRequest.password = this.loginForm.get('password').value;
    this.loginRequest.username = this.loginRequest.username.toLowerCase();
    
    this.authService.login(this.loginRequest).subscribe(
      data=>{
        this.toaster.success('login successful');
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
          this.router.navigate(['/home']); // navigate to same route
      }); 
          // this.router.navigate(['/home']); 
      },
      error=>{
        this.toaster.error('Please Enter Valid Credentials')
      }
    )
    
  }

  signUp(){
    this.router.navigateByUrl('/sign-up')
  }

}


// .type('Ufission')
//       .rest(500)
//       .changeOps({ deleteSpeed: 100 })
//       .remove(18)
//       .type('Future Within You')
//       .rest(500)
//       .remove(23)
//       .type('More than just a Future')
//       .rest(500)
//       .changeOps({ deleteSpeed: 60 })
//       .remove(23)
//       .rest(500)
//       .type('Just for you..')
//       .rest(500)
//       .clear()
//       .start()
