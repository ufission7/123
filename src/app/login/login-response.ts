export interface LoginResponse
{
    token : String,
    expireTime : Date,
    refreshToken : String,
    username : String
}