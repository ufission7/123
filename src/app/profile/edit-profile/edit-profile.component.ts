import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HomeService } from 'src/app/home/home.service';
import { AuthService } from 'src/app/shared/auth.service';
import { ProfileService } from '../profile.service';
import { UserDetails } from './userDetailsResponse';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  username:string;
adminRole:boolean;
emailId:string;
phoneNo:number;
name:string;
userDetails:UserDetails;
  constructor(private authService:AuthService,private homeService:HomeService,private profileService:ProfileService,private toaster:ToastrService) {
  this.username = this.authService.getUserName();
    this.homeService.getUserRole(this.authService.getUserName()).subscribe(data=>{
      this.adminRole=data;
      console.log(this.adminRole)
    });
    this.profileService.getUserDetails(this.username).subscribe(data=>{
      console.log(data)
     this.emailId = data['email'];
     this.phoneNo = data['phoneNum'];
     this.name = data['name'];
    })
   }

  ngOnInit() {
    this.userDetails={
      username:'',
      name:'',
      phoneNum:undefined,
      email:''
    }
  }

  saveChanges()
  {
    this.userDetails.email = this.emailId;
    this.userDetails.phoneNum = this.phoneNo;
    this.userDetails.name = this.name;
    this.userDetails.username = this.username;
    console.log(this.userDetails)
    this.profileService.updateUserDetails(this.userDetails).subscribe(data=>{
      this.toaster.success('Updated Details Successfully');
    },(error)=>{
      this.toaster.error('Error while updating details , please retry!')
      console.log(error)
    })
  }
}
