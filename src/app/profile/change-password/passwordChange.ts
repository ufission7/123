export interface PasswordChange
{
    username:string;
    currentPass:string,
    newPass:string,
}