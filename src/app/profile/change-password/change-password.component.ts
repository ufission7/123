import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HomeService } from 'src/app/home/home.service';
import { AuthService } from 'src/app/shared/auth.service';
import { UserDetails } from '../edit-profile/userDetailsResponse';
import { ProfileService } from '../profile.service';
import { PasswordChange } from './passwordChange';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

username:string;
adminRole:boolean;
passChangeForm:FormGroup;
passChangeRequest:PasswordChange;
  constructor(private authService:AuthService,private homeService:HomeService,private profileService:ProfileService,private toaster:ToastrService) {
  this.username = this.authService.getUserName();
    this.homeService.getUserRole(this.authService.getUserName()).subscribe(data=>{
      this.adminRole=data;
      console.log(this.adminRole)
    });
    this.passChangeRequest={
      username:'',
      currentPass:'',
      newPass:''
    };
  }

  ngOnInit() {
    this.passChangeForm = new FormGroup({
      currentPass:new FormControl(''),
      newPass:new FormControl(''),
      confirmPass:new FormControl('')
    })
  }

  passChange()
  {
    if(this.checkValidRequest())
    {
    this.passChangeRequest.username = this.username;
    this.passChangeRequest.currentPass = this.passChangeForm.get('currentPass').value;
    this.passChangeRequest.newPass = this.passChangeForm.get('newPass').value;
  
    this.profileService.changePassword(this.passChangeRequest).subscribe(data=>{
      if(data==true)
      {
       
      this.toaster.success("Password Changed Successfully")
      }
      else 
      this.toaster.error("Current Password is wrong")

    })

  }
}
  checkValidRequest():boolean
  {
    if(this.passChangeForm.get('newPass').value!==this.passChangeForm.get('confirmPass').value)
    {
    this.toaster.error("Password and Confirm Password didnt match")
    return false;
    }
    return true;

  }

}
