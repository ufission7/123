import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PasswordChange } from './change-password/passwordChange';
import { UserDetails } from './edit-profile/userDetailsResponse';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private httpClient:HttpClient) { }

  getUserDetails(username:string):Observable<any>
  {
return this.httpClient.get(environment.api_url+'/acc/getUserDetails?username='+username);
  }
  updateUserDetails(userDetails:UserDetails):Observable<any>
  {
return this.httpClient.post(environment.api_url+'/acc/updateUserDetails',userDetails,{ responseType:'text'});
  }
  changePassword(passwordChange:PasswordChange):Observable<any>
  {
   
    return this.httpClient.post(environment.api_url+'/acc/changePassword',passwordChange);
  }

}
