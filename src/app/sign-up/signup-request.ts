import { ɵangular_packages_platform_browser_dynamic_platform_browser_dynamic_a } from '@angular/platform-browser-dynamic';
import { SignUpComponent } from "./sign-up.component";

export interface SignUpRequest{
    username : string,
    name:string,
    password : string,
    confirmPassword : string,
    gender : string,
    email : string,
    phoneNum : number,
    aadharNum : number,
    birthday : string,
    referal:string
}