import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { AuthService } from '../shared/auth.service';
import { SignUpRequest } from './signup-request';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  signupForm :FormGroup
  signupRequest : SignUpRequest
  fileToUpload: File = null;
isInValidUsername:string;
isInvalidAadhar:boolean;
passHide = true;
confirmpassHide=true;

  constructor(private authService:AuthService,private router:Router,private toaster:ToastrService) {
    this.signupRequest={
      username: '',
      name:'',
      password : '',
      confirmPassword : '',
      email : '',
      gender : '',
      phoneNum : undefined,
      aadharNum : undefined,
      birthday : undefined,
      referal:'',
     
    }
   }  

  ngOnInit() {
    this.signupForm = new FormGroup({
      username : new FormControl('',Validators.required),
      email : new FormControl('',[Validators.required,Validators.email]),
      password : new FormControl('',Validators.required),
      confirmPassword : new FormControl('',Validators.required),
      gender : new FormControl('',Validators.required),
      phoneNum : new FormControl('',Validators.required),
      aadharNum : new FormControl('',Validators.required),
      birthday : new FormControl('',Validators.required),
      name : new FormControl('',Validators.required),
      referal:new FormControl({ value: "", disabled: true }, [
        Validators.required
      ])
    })
    
  }

  signup()
  {

    
    this.signupRequest.username = this.signupForm.get('username').value;
    this.signupRequest.password = this.signupForm.get('password').value;
    this.signupRequest.confirmPassword = this.signupForm.get('confirmPassword').value;
    this.signupRequest.birthday = this.formatDate(this.signupForm.get('birthday').value);
    this.signupRequest.email = this.signupForm.get('email').value;
    this.signupRequest.phoneNum = this.signupForm.get('phoneNum').value;
    this.signupRequest.gender = this.signupForm.get('gender').value;
    this.signupRequest.aadharNum = this.signupForm.get('aadharNum').value;
    this.signupRequest.name = this.signupForm.get('name').value;
   
   
if(!this.checkValidSignUp(this.signupRequest)) 
{
  return;
}
if(this.signupRequest.aadharNum.toString().length==16)
{

  this.authService.checkAadharExist(this.signupRequest.aadharNum.toString()).subscribe(data=>{
   
    if(data==true)
    {
      this.toaster.error("User with this aadharnum already exists")

    }
    else{
    this.addUser();
    }
  });
}

  }

  addUser()
  {
    this.authService.checkUserExist(this.signupRequest.username).subscribe(data=>{
  
      if(data==false){
        this.authService.postFile(this.signupRequest,this.fileToUpload);
          
        if(this.signupForm.get('referal').value){
         
        var str =  this.signupForm.get('referal').value;
       
      var n = str.lastIndexOf('?');
      
      var referal_id = str.substring(n + 1);
      
      this.signupRequest.referal = referal_id;
      
        }
        else
        this.signupRequest.referal = 'NA';
      
        this.authService.signup(this.signupRequest)
        .subscribe(
          ()=>{
            this.toaster.success('signUp success');
            
          this.router.navigate(['/login']);
        },
          (error)=>{
           
            this.toaster.error("Registration failed");
        });
      }
      else
      {
        this.toaster.error("Username already taken");
      }
      });
      
         
      }
  

  formatDate(date: string): string // from internal model -> your mode
  {
    var date1 = new Date(date),
    mnth = ("0" + (date1.getMonth() + 1)).slice(-2),
    day = ("0" + date1.getDate()).slice(-2);
  
 return [date1.getFullYear(), mnth, day].join("-");

    // return date?date.year+"/"+('0'+date.month).slice(-2)
    //        +"/"+('0'+date.day).slice(-2):null
  }
   
handleFileInput(files: FileList) {
  this.fileToUpload = files.item(0);
}

checkValidSignUp(signUpDetails:SignUpRequest):boolean
{
  var username = signUpDetails.username.toLowerCase();

  var alphaRegex = /^[a-z0-9]+$/i;

  // var passwordRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

  

  if(signUpDetails.phoneNum.toString().length!=10)
  {
    this.toaster.error("Enter Valid PhoneNumber")
    return false;
  }
  if(signUpDetails.aadharNum.toString().length!=12)
  {
    this.toaster.error("Enter 12 digit AadharNumber")
    return false;
  }

 if(signUpDetails.password.length<8)
 {
   this.toaster.error("Password should contain 8 characters")
   return false;
 }

  

  if(signUpDetails.password!==signUpDetails.confirmPassword)
  {
  this.toaster.error("Password and Confirm Password didnt match")
return false;
  }
 
  if(!(alphaRegex.test(username)))
{
  this.toaster.error("Username should not contain characters")
  return false;
}

return true;

}


}
